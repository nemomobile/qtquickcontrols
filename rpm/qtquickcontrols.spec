Name:       qt5-qtquickcontrols
Summary:    QtQuick Controls library
Version:    5.12.7
Release:    0
Group:      System/Libraries
License:    LGPLv2.1 with exception or GPLv3
URL:        https://www.qt.io
Source0:    %{name}-%{version}.tar.bz2
BuildRequires:  pkgconfig(Qt5Core)
BuildRequires:  pkgconfig(Qt5Qml)
BuildRequires:  pkgconfig(Qt5Quick)
BuildRequires:  pkgconfig(Qt5Gui)
BuildRequires:  qt5-qtdeclarative-devel-tools

%description
The Qt Quick Controls module provides a set of controls that can be
used to build complete interfaces in Qt Quick.

%prep
%setup -q -n %{name}-%{version}/upstream

%build
export QTDIR=/usr/share/qt5
touch .git

%qmake5
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
%qmake5_install

%files
%defattr(-,root,root,-)
%{_libdir}/qt5/qml/QtQuick/Controls/
%{_libdir}/qt5/qml/QtQuick/Dialogs/
%{_libdir}/qt5/qml/QtQuick/Extras/